@extends('layout.master')
@section('title')
    <h1>Buat Account Baru!</h1>  
@endsection


@section('content')
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label >Nama Depan</label><br>
        <input type="text" name="fname"><br><br>
        <label >Nama Tengah</label><br>
        <input type="text" name="mname"><br><br>
        <label >Nama Belakang</label><br>
        <input type="text" name="lname"><br><br>
        
        <label>Gender:</label><br><br> 
        <input type="radio" name="Gender" value="1" > Male <br>
        <input type="radio" name="Gender" value="1" > Female <br>
        <input type="radio" name="Gender" value="1" > Male <br><br>

        <label>Nationality:</label><br><br>
            <label for="Nationality"></label>
            <select name="Nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Thailand">Thailand</option>
            <option value="Korea">Korea</option>
            <option value="China">China</option>
            <option value="Afrika">Afrika</option>
        </select><br><br>

        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="Bahasa Indonesia"> Bahasa Indonesia<br>
        <input type="checkbox" name="English"> English<br> 
        <input type="checkbox" name="Other"> Other<br><br>

        <label>Bio:</label><br><br>
        <textarea cols="30" rows="10"></textarea> <br><br>

        
         <input type="submit" value="Sign up">
    </form>
    @endsection