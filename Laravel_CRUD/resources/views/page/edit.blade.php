@extends('layout.master')
@section('title')
   halaman tambah cast
@endsection


@section('content')
<form method="POST" action="/cast">
    @csrf
        <div class="form-group">
            <label >Cast name</label>
            <input type="text" name="nama" class="form-control" >
        </div>
        @error('nama')
            <div class="alert alert-danger">{{$message}} </div>
        @enderror
        <div class="form-group">
            <label >umur</label>
            <input type="integer" name="umur" class="form-control" >
        </div>
        @error('umur')
            <div class="alert alert-danger">{{$message}} </div>
        @enderror
        <div class="form-group">
            <label >bio</label>
            <input type="text" name="bio" class="form-control" >
        </div>
        @error('bio')
            <div class="alert alert-danger">{{$message}} </div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
  </form>
    @endsection