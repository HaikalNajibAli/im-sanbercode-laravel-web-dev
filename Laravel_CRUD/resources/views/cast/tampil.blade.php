@extends('layout.master')
@section('title')
   halaman Tampil cast
@endsection


@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm my-2 ">Tambah</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">NP</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                </td>
            </tr>
        @empty
            
        @endforelse
      
    </tbody>
  </table>

@endsection