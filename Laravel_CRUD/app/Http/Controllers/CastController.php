<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view ('cast.tambah');
    }

    public function store(Request $request)
    {
       //Validasi data
       $request->validate([
        'nama' => 'required|min:5',
        'umur' => 'required|min:1',
        'bio' => 'required|min:10',
       ]);

       //MASUKKAN data request ke table cast di database
       DB::table('cast')->insert([
        'nama' => $request['nama'],
        'umur' => $request['umur'],
        'bio' => $request['bio'],
       ]);

       //kita lempar ke halaman /cast
       return redirect('/cast');
       }

       public function index()
       {
        $cast = DB::table('cast')->get();
        //dd($cast);

        return view('cast.tampil', ['cast' => $cast]);
    }

        public function show($id)
        {
            $cast = DB::table('cast')->find($id);

            return view('cast.detail', ['cast' => $cast]);
        }
        public function edit($id)
        { 
            $cast = DB::table('cast')->find($id);

            return view ('cast.edit', ['cast' => $cast]);
        }

        }

