<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registerr()
    {
        return view('page.register');
    }

    public function welcomee(Request $request)
    {
        $namaDepan = $request['fname'];
        $namaTengah = $request['mname'];
        $namaBelakang = $request['lname'];

        
        return view('page.welcome', ["namaDepan" => $namaDepan,"namaTengah" => $namaTengah ,'namaBelakang' => $namaBelakang]);
    }
}
